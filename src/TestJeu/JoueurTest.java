package TestJeu;


import java.io.IOException;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.*;
import modele.Joueur;

public class JoueurTest {
	Joueur j;
	
	@Before
	public void initialiser() throws IOException {
		j= new Joueur("Leo", 5);
		j.setScore(2);
	}
	@Test
	public void testScoreJoueur() {
		assertEquals("le score doit �tre �gal � 2", 2, j.getScore());
	}
	@Test
	public void testAgeJoueur() {
		assertEquals("l'age du joueur doit �tre �gal � 5", 5, j.getAge());
	}
	@Test
	public void testPseudoJoueur() {
		assertEquals("le joueur s'appelle Leo", "Leo", j.getPseudo());
	}
	@Test
	public void testNbCartesJoueur() {
		assertEquals("le nombre de cartes du joueur avant de distribuer est 0", 0, j.getCartes().taille());
	}

}
