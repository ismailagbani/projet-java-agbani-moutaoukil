package TestJeu;


import java.io.IOException;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.*;
import modele.Jeu;

public class JeuTest {
	Jeu j;
	
	@Before
	public void initialiser() throws IOException {
		j= new Jeu("cardline", "PIB");
		j.setNbJoueurs(2);
	}
	@Test
	public void testNombreJoueurs() {
		assertEquals("le nombre de joueurs doit �tre �gal � 2", 2, j.getNbJoueurs());
	}
	
	@Test
	public void testType() {
		assertEquals("le type est cardline", "cardline", j.getCritereJeu());
	}
	
	@Test
	public void testCritere() {
		assertEquals("le critere est pib", "PIB", j.getTypeJeu());
	}
	
	@Test
	public void testNbCartesTable() {
		assertEquals("au debut il n'y a aucune carte avant la distribution", 0, j.getTables().taille());
	}

}
