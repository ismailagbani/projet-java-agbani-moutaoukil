package Controleurs;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import Vues.VueCarte;
import modele.Card;

public class ControleurCarte implements MouseListener {
	/**
	 * attribut vuecarte de la classe
	 */
	private VueCarte c;
	
	/*
	 * attribut permettant d'avoir la carte selectionnee
	 */
	private static Card carteSelectionnee;
	
	/**
	 * Contructeur prenant en paramétre une vue carte
	 * sur laquelle les actions seront executees
	 * @param carte
	 */
	public ControleurCarte(VueCarte carte) {
		this.c=carte;
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		c.getGraphics().setColor(Color.RED);
		System.out.println(c.getCarte());
		this.carteSelectionnee=c.getCarte();
		c.getGraphics().drawRect(0, 42, 182, 301);
		c.getGraphics().setColor(Color.RED);
		c.revalidate();
	}

	
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public static Card getCarteSelectionnee() {
		return carteSelectionnee;
	}
	
	public void setCarteSelectionnee(Card carteSelectionnee) {
		this.carteSelectionnee = carteSelectionnee;
	}
	
}
