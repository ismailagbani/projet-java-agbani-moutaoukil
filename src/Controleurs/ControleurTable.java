package Controleurs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import Vues.VueCarteTable;
import Vues.VueJeu;
import modele.Card;
import modele.Jeu;

public class ControleurTable implements ActionListener {
	private VueCarteTable vueTable;
	private Jeu jeu;
	private VueJeu vj;
	
	
	public ControleurTable(VueCarteTable vueTable, Jeu j, VueJeu vueJeu) {
		super();
		this.vueTable = vueTable;
		this.jeu=j;
		this.vj=vueJeu;
	}




	@Override
	public void actionPerformed(ActionEvent e) {
		
		// TODO Auto-generated method stub
		int endroit;
		System.out.println("taille tables zzzz avanaaaaant "+jeu.getTables().taille());
		System.out.println("controleur bouton");
		System.out.println(this.vueTable.getButton1().getText());
		System.out.println(this.vueTable.getButton2().getText());
		//System.out.println(((JRadioButton)(e.getSource())).getText() + "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
		endroit=Integer.parseInt(((JRadioButton)(e.getSource())).getText());
		System.out.println(ControleurCarte.getCarteSelectionnee());
		Card carteSelectionneeParJoueur=ControleurCarte.getCarteSelectionnee();
		int indCarte=jeu.getJoueurAyantTour().getCartes().indexOfCarte(carteSelectionneeParJoueur);
		System.out.println(indCarte);
		if (jeu.joueurJoue(indCarte, endroit)) {
			JOptionPane.showMessageDialog(null, "Le gagnant est le joueur "+this.jeu.getJoueurAyantTour().getPseudo()+" avec un score de "+this.jeu.getJoueurAyantTour().getScore()+" points.", "Bravo", JOptionPane.WARNING_MESSAGE);
		}	
		
		System.out.println("taille tables zzzz "+jeu.getTables().taille());
		
		this.vj.revalidate();
		this.vj.repaint();
		this.vj.mettreAJourPanelTable();
		this.vueTable.getGroupeButton().clearSelection();
		this.vj.mettreAJourPanelJoueur();
	
		

	}

}
