package modele;

/**
 * Classe representant les cartes de type Invention
 */
public class CarteInvention extends Card {
	
	/**
	 * attribut nom de l'invention
	 */
	private String Invention;
	
	/**
	 * attribut date de l'invention
	 */
	private String Date;
	
	/**
	 * Constructeur prenant en compte 3 parametres
	 * @param invention String nom de l'invention
	 * @param date String date de l'invention
	 * @param img String representant l'image de la carte
	 */
	public CarteInvention(String invention, String date, String img) {
		super();
		Invention = invention;
		Date = date;
		super.image=img;
	}

	public String getInvention() {
		return Invention;
	}

	public void setInvention(String invention) {
		Invention = invention;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	@Override
	public String toString() {
		return "CarteInvention [Invention=" + Invention + ", Date=" + Date + ", image=" + image + "]";
	}


	
	
	
}
