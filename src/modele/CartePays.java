package modele;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;

import javax.imageio.ImageIO;

/**
 * Classe modelisant les cartes de type Pays
 */
public class CartePays extends Card {
	private String ville;
	private String superficie;
	private String population;
	private String pib;
	private String pollution;
	
	/**
	 * Constructeur de la classe
	 * @param ville String representant le nom de la carte
	 * @param superficie String représentant la superficie du pays
	 * @param population String représentant le nombre d'habitant 
	 * @param pib String représentant le PIB
	 * @param pollution String représentant le taux de pollution
	 * @param image String représentant l'image du pays
	 */
	public CartePays(String ville, String superficie, String population, String pib, String pollution, String image) {
		super();
		this.ville = ville;
		this.superficie = superficie;
		this.population = population;
		this.pib = pib;
		this.pollution = pollution;
		super.image = image;
	}
	
	
	
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getSuperficie() {
		return superficie;
	}
	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}
	public String getPopulation() {
		return population;
	}
	public void setPopulation(String population) {
		this.population = population;
	}
	public String getPib() {
		return pib;
	}
	public void setPib(String pib) {
		this.pib = pib;
	}
	public String getPollution() {
		return pollution;
	}
	public void setPollution(String pollution) {
		this.pollution = pollution;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	public int comparerCarte(CartePays carte2, String critereJeu) {
		int res=0;
		switch(critereJeu) {
		case "Superficie" : 
			Integer i = Integer.parseInt(this.getSuperficie());
			Integer i2 = Integer.parseInt(carte2.getSuperficie());
			res= i.compareTo(i2);
			break;
		case "Pollution" :
			String s = this.getPollution().replace(",", ".");
			String s2 = carte2.getPollution().replace(",", ".");
			double i3 = Double.parseDouble(s);
			double i4 = Double.parseDouble(s2);
			res=Double.compare(i3,i4); 
			break;
		case "Population" : 
			Integer i5 = Integer.parseInt(this.getPopulation());
			Integer i6 = Integer.parseInt(carte2.getPopulation());
			res=i5.compareTo(i6);
			break;
		case "PIB" :
			Integer i7 = Integer.parseInt(this.getPib());
			Integer i8 = Integer.parseInt(carte2.getPib());
			res=i7.compareTo(i8); 
			break;
		}
		System.out.println(res + "klllllllllllklkkkklklklkl");
		return res;
	}
	
	@Override
	public String toString() {
		return "Carte [ville=" + ville + ", superficie=" + superficie + ", population=" + population + ", pib=" + pib
				+ ", pollution=" + pollution + ", image=" + image + "]";
	}

	
}
