package modele;
import java.io.Serializable;

/**
 * Classe representant un joueur
 */
public class Joueur implements Serializable, Comparable {

	/**
	 * attribut permettant d'avoir les cartes du joueurs
	 */
	private Cartes cartes;
	
	/**
	 * attribut pseudo du joueur
	 */
	private String pseudo;
	
	/**
	 * attribut age du joueur
	 */
	private int age;
	
	/**
	 * attribut permettant d'avoir le score du joueur
	 */
	private int score;
	
	/**
	 * attribut a true si le joueur a le tour
	 */
	private boolean tour;
	
	/**
	 * attribut a true si le joueur a utilise son joker
	 */
	private boolean jokerUtilise;
	
	/**
	 * Constructeur
	 * @param n nom du joueur
	 * @param a age du joueur
	 */
	public Joueur(String n, int a) {
		this.pseudo=n;
		this.age=a;
		this.cartes=new Cartes();
		this.score=0;
		this.tour=false;
		this.jokerUtilise=false;
	}

	public Cartes getCartes() {
		return cartes;
	}

	public void setCartes(Cartes cartes) {
		this.cartes = cartes;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

		
	public boolean getTour() {
		return tour;
	}

	public void setTour(boolean tour) {
		this.tour = tour;
	}

	@Override
	public String toString() {
		return "Joueur [cartes=" + cartes + ", pseudo=" + pseudo + ", age=" + age + ", score=" + score + "]";
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + score;
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Joueur other = (Joueur) obj;
//		if (score != other.score)
//			return false;
//		return true;
//	}

	@Override
	public int compareTo(Object arg0) {
		System.out.println(this.score-((Joueur)arg0).getScore());
		return this.score-((Joueur)arg0).getScore();
		
	}

	public boolean isJokerUtilise() {
		return jokerUtilise;
	}

	public void setJokerUtilise(boolean jokerUtilise) {
		this.jokerUtilise = jokerUtilise;
	}
	

}
