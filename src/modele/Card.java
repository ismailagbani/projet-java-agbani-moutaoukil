package modele;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

/**
 *classe abstraite des cartes
 */
public abstract class Card implements Serializable {
	public String image;
	private boolean selected;
	private int posX;
	private int posY;
	
	public Image lireImage(String file) throws IOException {
		BufferedImage imag = ImageIO.read(new File(file+this.image+".jpeg"));
		return imag;
		
	}
	
	public Image lireImageReponse(String file) throws IOException {
		BufferedImage imag = ImageIO.read(new File(file+this.image+"_reponse.jpeg"));
		return imag;
		
	}
	
	public Image lireImageDate(String file) throws IOException {
		BufferedImage imag = ImageIO.read(new File(file+this.image+"_date.jpeg"));
		return imag;
		
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}
	
}
