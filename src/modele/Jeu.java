package modele;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Classe repr�sentant l'ensemble du jeu
 */
public class Jeu implements Serializable {


	private int nbJoueurs;
	private String critereJeu;
	private List<Joueur> lesJoueurs;
	private Joueur joueurAyantTour;
	// toutes les cartes du deck
	private Cartes cartes;
	// cartes jou�es
	private Cartes cartesSales;

	// table de cartes
	private Cartes tables;

	//nombre de cartes jou�es
	private int nbCartesJouees;
	private transient Scanner sc = new Scanner(System.in);

	private boolean finDuJeu;

	private String typeJeu;


	/**
	 * Constructeur
	 * @param critere en fonction du type choisi
	 * @param type Choix cardline ou timeline
	 */
	public Jeu(String critere, String type) {
		lesJoueurs = new ArrayList<Joueur>();
		//		for (int i=0; i<nbj; i++) {
		//			String pseudo = sc.next();
		//			int age=sc.nextInt();
		//			Joueur j = new Joueur(pseudo, age);
		//			lesJoueurs.add(j);
		//		}
		//		this.nbJoueurs=nbj;
		this.critereJeu=critere;
		this.cartes=new Cartes();
		this.cartesSales=new Cartes();
		this.tables=new Cartes();
		this.nbCartesJouees=1;
		this.typeJeu=type;
		this.finDuJeu = false;
		System.out.println(this.typeJeu);

	}
	/**
	 * methode permettant de charger les cartes
	 * @throws IOException
	 */
	public void chargerCartes() throws IOException {

		String choixJeu = typeJeu;
		System.out.println("liste des joueurs "+ this.lesJoueurs);
		if (choixJeu.equals("timeline")) {
			Data.lireFichierCartes("data/timeline/timeline.csv");
			for (int i=1; i<Data.listeCartesInvention.size(); i++) {
				cartes.chargerCarte(Data.listeCartesInvention.get(i));

			}

		} else if (choixJeu.equals("cardline")) {
			Data.lireFichierCartes("data/cardline/cardline.csv");
			for (int i=1; i<Data.listeCartesPays.size(); i++) {
				cartes.chargerCarte(Data.listeCartesPays.get(i));

			}
		}
		System.out.println(cartes);
	}

	/**
	 * methode permettant de charger les cartes dans la table
	 */
	public void chargerCarteTables() {
		System.out.println("taille cartes avant de remplir la table "+ cartes.taille());
		if (this.tables.taille()==0) {
			this.tables.chargerCarte(cartes.tirerUneCarte());

		}
		System.out.println("tables : "+ tables);
		System.out.println("tables nbcartes : " + tables.taille());
		System.out.println("taille cartes apres avoir rempli la table : "+ cartes.taille());
	}

	/**
	 * methode permettant de distribuer les cartes aux joueurs
	 * en fonction du nombre de joueurs inscrits
	 */
	public void distribuerCartesJoueurDebutJeu() {
		System.out.println("taille cartes avant de distribuer les cartes au joueur "+ cartes.taille());
		int nbcartesjoueurs=0;
		// a remettre en place quand le fait de jouer a plusieurs est pris en compte
		if (nbJoueurs<=3 && nbJoueurs>=2) {
			nbcartesjoueurs=6;
		} else if (nbJoueurs<=5 && nbJoueurs>=4) {
			nbcartesjoueurs=5;
		} else if (nbJoueurs>=6 && nbJoueurs<=8) {
			nbcartesjoueurs=4;
		}
		for (int i=0; i<nbcartesjoueurs; i++) {
			for (int j=0; j<this.lesJoueurs.size(); j++) {
				this.lesJoueurs.get(j).getCartes().chargerCarte(cartes.tirerUneCarte());
			}

		}
		//System.out.println("carte joueur0 : "+this.lesJoueurs.get(0).getCartes());
		System.out.println("taille cartes apres la distribution au joueur "+ cartes.taille());
	}


	/**
	 * methode permettant de faire jouer le joueur ayant le tour
	 * @param idCarte carte selectionnee par le joueur a poser
	 * @param endroit enndroit o� le joueur souhaite d�poser la carte
	 * @return un booleen, true si le joueur depose la carte au bon endroit, sinon false
	 */
	public boolean joueurJoue(int idCarte, int endroit) {
		boolean gagnant=false;
		//		while(!finDuJeu) {
		int idJoueur = this.getLesJoueurs().indexOf(joueurAyantTour);
		//System.out.println("carte a poser" + this.lesJoueurs.get(idJoueur).getCartes().getCarte(idCarte).toString());
		System.out.println("taille deck deb " + cartes.taille() );
		Joueur joueurMain =  this.lesJoueurs.get(idJoueur);
		boolean ajouterCarteTable = true;
		int nbCartesTable = this.tables.taille();
		System.out.println("cartes tables : "+this.tables.taille());
		System.out.println("cartes joueurs : "+ this.lesJoueurs.get(idJoueur).getCartes().taille());
		Card carteTiree=this.lesJoueurs.get(idJoueur).getCartes().tirerCarteJoueur(idCarte);
		//this.tables.chargerCarte(carteTiree);
		System.out.println("Nombre de cartes sur le plateau du jeu " + this.nbCartesJouees);
		String jeuTable ="";
		for(int i=1; i<=this.nbCartesJouees+1;i++) {
			System.out.print("(" + i + ")" + "      ");
			jeuTable+="(" + i + ")" + "      ";
			if(i<=this.nbCartesJouees) {
				System.out.print("carte");
				jeuTable+="carte";
			}
			System.out.print("          ");
			jeuTable+="          ";
		}

		System.out.println("O� voulez-vous poser la carte ?");
		//int endroit = sc.nextInt();
		System.out.println(this.typeJeu);
		if(this.typeJeu.contains("timeline")) {
			System.out.println(((CarteInvention)carteTiree).getDate() + "    111111111111111111111111");
			System.out.println(((CarteInvention)this.tables.getCarte(0)).getDate()+ "       2222222222222222222222222");
			System.out.println("ytytytyttytyt" + ((CarteInvention)carteTiree).getDate().compareTo(((CarteInvention)this.tables.getCarte(0)).getDate()));
			if(endroit==1) {
				String date = ((CarteInvention)carteTiree).getDate();
				String date2 = ((CarteInvention)this.tables.getCarte(0)).getDate();
				Integer integerdate = Integer.parseInt(date);
				Integer integerdate2 = Integer.parseInt(date2);
				int diff=integerdate.compareTo(integerdate2);
				if(diff>0) {
					System.out.println("ereeeeeeeeeur"); 
					cartes.getLesCartes().add(carteTiree);
					joueurMain.getCartes().chargerCarte(cartes.getCarte(0));
					cartes.retirerUneCarte(1);
					ajouterCarteTable = false;
				}

			}
			else {
				if(endroit==this.tables.taille()+1) {
					String date = ((CarteInvention)carteTiree).getDate();
					String date2 = ((CarteInvention)this.tables.getCarte(this.tables.taille()-1)).getDate();
					Integer integerdate = Integer.parseInt(date);
					Integer integerdate2 = Integer.parseInt(date2);
					int diff=integerdate.compareTo(integerdate2);
					if(diff<0) {
						System.out.println("ereeeeeeeeeeeeeeeeeur");
						cartes.getLesCartes().add(carteTiree);
						joueurMain.getCartes().chargerCarte(cartes.getCarte(0));
						cartes.retirerUneCarte(1);
						ajouterCarteTable = false;

					}
				}
				else {
					String date = ((CarteInvention)carteTiree).getDate();
					String date2 = ((CarteInvention)this.tables.getCarte(endroit-2)).getDate();
					String date3 = ((CarteInvention)this.tables.getCarte(endroit-1)).getDate();

					Integer integerdate = Integer.parseInt(date);
					Integer integerdate2 = Integer.parseInt(date2);
					Integer integerdate3 = Integer.parseInt(date3);


					int diff=integerdate.compareTo(integerdate2);
					int diff2=integerdate.compareTo(integerdate3);
					System.out.println(diff + "    " + diff2);
					if(diff<0 | diff2>0) {
						System.out.println("ereurrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
						cartes.getLesCartes().add(carteTiree);
						joueurMain.getCartes().chargerCarte(cartes.getCarte(0));
						cartes.retirerUneCarte(1);
						ajouterCarteTable = false;
					}
				}
			}
		}
		else {
			//				System.out.println("carte a poser superficie " + ((CartePays)(this.lesJoueurs.get(idJoueur).getCartes().getCarte(idCarte))).getSuperficie());

			if(endroit==1) {
				int diff=((CartePays)carteTiree).comparerCarte(((CartePays)this.tables.getCarte(0)),critereJeu);
				System.out.println(diff + "jjjjjjjjjjjjjjjjjjjjjjjjj");
				if(diff>0) {
					System.out.println("cardline eeeeeerrrrrreeeeeeeeuuuur");
					cartes.getLesCartes().add(carteTiree);
					joueurMain.getCartes().chargerCarte(cartes.getCarte(0));
					cartes.retirerUneCarte(1);
					ajouterCarteTable = false;
				}
			}
			else {
				if(endroit==this.tables.taille()+1) {
					int diff=((CartePays)carteTiree).comparerCarte(((CartePays)this.tables.getCarte(this.tables.taille()-1)),critereJeu);
					System.out.println(diff + "jjjjjjjjjjjjjjjjjjjjjjjjj");
					if(diff<0) {
						System.out.println("cardline errrerererererrereeur");
						cartes.getLesCartes().add(carteTiree);
						joueurMain.getCartes().chargerCarte(cartes.getCarte(0));
						cartes.retirerUneCarte(1);
						ajouterCarteTable = false;


					}
				}
				else {
					int diff=((CartePays)carteTiree).comparerCarte(((CartePays)this.tables.getCarte(endroit-2)),critereJeu);
					int diff2=((CartePays)carteTiree).comparerCarte(((CartePays)this.tables.getCarte(endroit-1)),critereJeu);
					if(diff<0 | diff2>0) {
						System.out.println("cardlineee ereurrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
						cartes.getLesCartes().add(carteTiree);
						joueurMain.getCartes().chargerCarte(cartes.getCarte(0));
						cartes.retirerUneCarte(1);
						ajouterCarteTable = false;

					}

				}
			}
		}

		System.out.println(ajouterCarteTable);
		if(ajouterCarteTable==true) {
			this.joueurAyantTour.setScore(this.joueurAyantTour.getScore()+1);
			this.tables.getLesCartes().add(endroit-1, carteTiree);
			this.nbCartesJouees++;

		}
		System.out.println(jeuTable);
		System.out.println("cartes tables : "+this.tables.taille());
		System.out.println("cartes joueurs : "+ this.lesJoueurs.get(idJoueur).getCartes().taille());
		System.out.println(this.tables);
		System.out.println(this.lesJoueurs.size());	
		System.out.println(this.tables.taille());
		System.out.println("taille deck fin " + cartes.taille() );

		String jeuTable2 ="";
		for(int i=1; i<=this.nbCartesJouees+1;i++) {
			System.out.print("(" + i + ")" + "      ");
			jeuTable2+="(" + i + ")" + "      ";
			if(i<=this.nbCartesJouees) {
				System.out.print("carte");
				jeuTable2+="carte";
			}
			System.out.print("          ");
			jeuTable2+="          ";
		}
		System.out.println(" ");
		for(int i=0;i<this.getNbJoueurs();i++) {
			if(this.lesJoueurs.get(i).getCartes().taille()==0) {
				gagnant=true;
				System.out.println("Le jeu est termin�, les gagnant est le joueur " + this.lesJoueurs.get(i).getPseudo());
			}
		}
		if (!gagnant) {
			this.passerTour();
		}
		return gagnant;
	}

	//	}

	public Cartes getTables() {
		return tables;
	}

	public void setTables(Cartes tables) {
		this.tables = tables;
	}

	/**
	 * methode permettant de sauvegarder le jeu en cours
	 * dans un fichier
	 * @throws IOException
	 */
	public void sauvegarderJeu() throws IOException {
		ObjectOutputStream buffered = new ObjectOutputStream(new FileOutputStream("sauvegardejeu"));
		buffered.writeObject(this);
		buffered.close();
	}

	/**
	 * methode permettant de charger le jeu
	 * precedemment sauvegarder
	 * @return le jeu charge
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Jeu chargerJeu() throws FileNotFoundException, IOException, ClassNotFoundException {
		Jeu res = null;
		String fichier = "sauvegardejeu";
		ObjectInputStream buffered = new ObjectInputStream(new FileInputStream(fichier));
		res = (Jeu)buffered.readObject();
		buffered.close();
		return res;	

	}

	/**
	 * methode permettant d'�tablir le classement des joueurs
	 * @return la liste des joueurs class�e
	 */
	public List<Joueur> classementJoueurs() {

		Collections.sort(lesJoueurs, Collections.reverseOrder());
		return lesJoueurs;
	}
	
	/**
	 * methode permettant d'attribuer le tour au joueur 
	 * le plus jeune lors du lancement
	 */
	public void attribuerPremierTour() {

		Joueur joueurAgemax=this.lesJoueurs.get(0);
		for (int i=1; i<this.lesJoueurs.size(); i++) {
			if (this.lesJoueurs.get(i).getAge()>joueurAgemax.getAge()) {
				joueurAgemax=this.lesJoueurs.get(i);
			}
			this.joueurAyantTour=joueurAgemax;
			this.joueurAyantTour.setTour(true);
		}
	}

	/**
	 * methode permettant de passer le tour
	 * si le dernier joueur a la main, le tour revient au premier
	 */
	public void passerTour() {

		if(this.lesJoueurs.contains(this.joueurAyantTour)) {
			int indTour=this.lesJoueurs.indexOf(this.joueurAyantTour);

			if (indTour==this.lesJoueurs.size()-1) {
				indTour=-1;
			}

			this.joueurAyantTour=this.lesJoueurs.get(indTour+1);
			this.joueurAyantTour.setTour(true);
		}
		System.out.println("joueur ayant tour : "+this.joueurAyantTour);
		System.out.println("joueuodkqksdofjsdifhhfh LCJQJDDLQDJQDIIQEOD : "+this.joueurAyantTour);


	}


	public int getNbJoueurs() {
		return nbJoueurs;
	}

	public void setNbJoueurs(int nbJoueurs) {
		this.nbJoueurs = nbJoueurs;
	}

	public List<Joueur> getLesJoueurs() {
		return lesJoueurs;
	}

	public void setLesJoueurs(ArrayList<Joueur> lesJoueurs) {
		this.lesJoueurs = lesJoueurs;
	}

	public String getTypeJeu() {
		return typeJeu;
	}

	public void setTypeJeu(String typeJeu) {
		this.typeJeu = typeJeu;
	}

	public Joueur getJoueurAyantTour() {
		return joueurAyantTour;
	}

	public void setJoueurAyantTour(Joueur joueurAyantTour) {
		this.joueurAyantTour = joueurAyantTour;
	}
	public Cartes getCartes() {
		return cartes;
	}

	public void setCartes(Cartes cartes) {
		this.cartes = cartes;
	}

	public String getCritereJeu() {
		return critereJeu;
	}

	public void setCritereJeu(String critereJeu) {
		this.critereJeu = critereJeu;
	}

	public Cartes getCartesSales() {
		return cartesSales;
	}

	public void setCartesSales(Cartes cartesSales) {
		this.cartesSales = cartesSales;
	}

	public int getNbCartesJouees() {
		return nbCartesJouees;
	}

	public void setNbCartesJouees(int nbCartesJouees) {
		this.nbCartesJouees = nbCartesJouees;
	}

	public Scanner getSc() {
		return sc;
	}

	public void setSc(Scanner sc) {
		this.sc = sc;
	}

	public boolean isFinDuJeu() {
		return finDuJeu;
	}

	public void setFinDuJeu(boolean finDuJeu) {
		this.finDuJeu = finDuJeu;
	}

	public void setLesJoueurs(List<Joueur> lesJoueurs) {
		this.lesJoueurs = lesJoueurs;
	}
}
