package modele;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Classe chargeant les donn�es sur les cartes en fonction du type
 */
public class Data {

	public static HashMap<Integer, CartePays> listeCartesPays=new HashMap<Integer, CartePays>();
	public static HashMap<Integer, CarteInvention> listeCartesInvention=new HashMap<Integer, CarteInvention>();
	private static int idCartePays=0;
	private static int idCarteInvention=0;
	

	// m�thode pour lire les cartes dans le fichier cardline.csv
	
		public static void lireFichierCartes(String fichier) throws IOException {
			FileReader f = new FileReader(fichier);
			BufferedReader b = new BufferedReader(f);
			String ligne;
			while((ligne=b.readLine())!=null) {
				String[] tab = ligne.split(";");
				if (fichier.equals("data/cardline/cardline.csv")) {
				CartePays c = new CartePays(tab[0], tab[1], tab[2], tab[3], tab[4], tab[5]);
				listeCartesPays.put(idCartePays, c);
				idCartePays++;
				} else if (fichier.equals("data/timeline/timeline.csv")){
				CarteInvention c = new CarteInvention(tab[0], tab[1], tab[2]);
				listeCartesInvention.put(idCarteInvention, c);
				idCarteInvention++;
				}
			}
			b.close();
			//System.out.println(listeCartesInvention);
			//System.out.println(listeCartesPays);
		}
}
