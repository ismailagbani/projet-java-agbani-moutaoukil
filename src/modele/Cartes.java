package modele;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * classe mod�lisant l'ensemble des cartes � piocher
 * avec lesquelles on joue etc et les op�rations
 */
public class Cartes implements Serializable {

	private ArrayList<Card> lesCartes;

	public Cartes() {
		this.lesCartes=new ArrayList<Card>();
	}

	/**
	 * methode permettant de retirer une carte du jeu
	 * @param id dont l'id correspond au parametre
	 */
	public void retirerUneCarte(int id) {
		this.lesCartes.remove(id-1);
	}

	/**
	 * methode permettant de charger une carte
	 * @param carte
	 */
	public void chargerCarte(Card carte) {
		this.lesCartes.add(carte);
	}


	public ArrayList<Card> getLesCartes() {
		return lesCartes;
	}

	public void setLesCartes(ArrayList<Card> lesCartes) {
		this.lesCartes = lesCartes;
	}

	/**
	 * methode permettant au joueur de tirer une carte
	 * @param id carte a tiree
	 * @return la carte tiree
	 */
	public Card tirerCarteJoueur(int id) {

		Card d= this.lesCartes.get(id);
		this.lesCartes.remove(id);
		return d;
	}
	
	/**
	 * methode permettant de tirer une carte du deck
	 * @return la carte tiree
	 */
	public Card tirerUneCarte() {
		Card tiree=this.lesCartes.get(this.lesCartes.size()-1);
		this.lesCartes.remove(this.lesCartes.size()-1);
		return tiree;
	}
	
	public int taille() {
		return this.lesCartes.size();
	}

	@Override
	public String toString() {
		return "Cartes [lesCartes=" + lesCartes + "]";
	}
	public Card getCarte(int i) {
		return this.lesCartes.get(i);

	}
	public int indexOfCarte(Card d) {
		return this.lesCartes.indexOf(d);
	}
}
