package Vues;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import modele.Card;
import modele.Jeu;

/**
 * Classe permettant de voir le jeu entier
 * Avec tous les jpanel necessaires
 * @author Utilisateur
 *
 */
public class VueJeu extends Vue{
	private Jeu jeu;
	private JPanel panelJoueur;
	private JPanel panelTable;
	private int nbButton;
	private JPanel vueJoueur;
	private JPanel vueClassement;
	private JFrame frame;
	JMenuBar barreMenu;
	
	JMenu menuFichier;
	JMenuItem itemSauvegarder;
	
	JMenu menuHelp;	
	JMenuItem itemAideTimeline;
	JMenuItem itemAideCardline;
	
	JMenu changerJeu;

	
	
	public VueJeu(Jeu jeu, JFrame fr) {
		this.jeu=jeu;


		this.frame=fr;
		this.barreMenu=new JMenuBar();
		
		this.menuFichier=new JMenu("Fichier");
		this.itemSauvegarder=new JMenuItem("Sauvegarder jeu");		
		this.itemSauvegarder.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					jeu.sauvegarderJeu();
					JOptionPane.showMessageDialog(null, "Le jeu a �t� sauvegard� avec succ�s.", "Sauvegarde", JOptionPane.INFORMATION_MESSAGE);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		});
		this.menuFichier.add(itemSauvegarder);
		
		this.menuHelp=new JMenu("Help");
		this.itemAideTimeline=new JMenuItem("R�gles Timeline");
		this.itemAideTimeline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					File f=new File("data/regle.pdf");
					Desktop bureau = Desktop.getDesktop();
					bureau.open(f);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
		
		this.itemAideCardline=new JMenuItem("R�gles Cardline");
		this.itemAideCardline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					File f=new File("data/reglecardline.pdf");
					Desktop bureau = Desktop.getDesktop();
					bureau.open(f);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
		
		this.menuHelp.add(itemAideTimeline);
		this.menuHelp.add(itemAideCardline);
		
		
		

		
		this.barreMenu.add(menuFichier);	
		this.barreMenu.add(menuHelp);	
		this.frame.setJMenuBar(barreMenu);
		
		this.nbButton=1;
		
		this.setLayout(new GridLayout(2,1));
		panelJoueur = new JPanel();
		panelTable=new JPanel();
		panelTable.setLayout(new FlowLayout(40));
		panelJoueur.setLayout(new FlowLayout(40));
	
		String nomJoueur=jeu.getJoueurAyantTour().getPseudo();
		String ageJoueur=Integer.toString(jeu.getJoueurAyantTour().getAge());
		String scoreJoueur=Integer.toString(jeu.getJoueurAyantTour().getScore());
		String nbCartes=Integer.toString(jeu.getJoueurAyantTour().getCartes().taille());
		vueJoueur = new VueJoueur(jeu, nomJoueur, ageJoueur, scoreJoueur, nbCartes);
		//panelJoueur.setLayout(new GridLayout(1, jeu.getJoueurAyantTour().getCartes().taille()+1));
	
		
		vueClassement = new VueClassement(jeu);
		
		
		JScrollPane jt = new JScrollPane(panelJoueur,  JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.add(jt);
		JScrollPane jp = new JScrollPane(panelTable,  JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.add(jp);
		
		
		//		jeu.getTables().getLesCartes().add(this.jeu.getJoueurAyantTour().getCartes().getCarte(2));
		//		jeu.getTables().getLesCartes().add(this.jeu.getJoueurAyantTour().getCartes().getCarte(3));
		panelJoueur.add(vueJoueur);
		panelTable.add(vueClassement);
		for (int j=0; j<this.jeu.getJoueurAyantTour().getCartes().taille(); j++) {
			Card carte=this.jeu.getJoueurAyantTour().getCartes().getCarte(j);
			VueCarte vc = new VueCarte(carte, 0, 0, jeu.getTypeJeu());
			panelJoueur.add(vc);

		}

		for (int j=0; j<this.jeu.getTables().taille(); j++) {
			Card carte=this.jeu.getTables().getCarte(j);
			VueCarteTable vTable = null;

			if(j==0) 				{
				vTable = new VueCarteTable(carte, 0, 0, jeu.getTypeJeu(), Integer.toString(this.nbButton),  Integer.toString(this.nbButton+1), jeu, this);
				this.nbButton++;

			}
			else {
				vTable = new VueCarteTable(carte, 0, 0, jeu.getTypeJeu(), Integer.toString(this.nbButton), jeu, this);
				this.nbButton++;
			}


			panelTable.add(vTable);


		}

	}

	public JPanel getPanelTable() {
		return panelTable;
	}

	private void setPanelTable(JPanel panelTable) {
		this.panelTable = panelTable;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);


		//		this.jeu.getLesJoueurs().get(0).setTour(true);
		//		for(int i=0; i<this.jeu.getNbJoueurs();i++) {
		//			if(this.jeu.getLesJoueurs().get(i).getTour()) {
		//		g.drawString("Pseudo Joueur : " + this.jeu.getJoueurAyantTour().getPseudo(), 8, 40);
		//		g.drawString("Age Joueur : " + this.jeu.getJoueurAyantTour().getAge(), 8, 70);
		//		g.drawString("Score Joueur : " + this.jeu.getJoueurAyantTour().getScore(), 8, 100);
		//		System.out.println(this.jeu.getJoueurAyantTour().getCartes().taille());


		// tables
		//		g.drawString("Nombres Cartes Table : " + this.jeu.getTables().taille(), 8, 340);
		//		for (int i=0; i<this.jeu.getTables().taille(); i++) {
		//			Card carteTable = this.jeu.getTables().getCarte(i);
		//			Image imgTable = lectureCarteTypeJeu(carteTable, "table");
		//			g.drawImage(imgTable, this.getWidth()/2, 380, 150, 270, null);
		//			xT+=160;
		//		}

	}




	public void mettreAJourPanelJoueur() {
		panelJoueur.removeAll();
		String nomJoueur=jeu.getJoueurAyantTour().getPseudo();
		String ageJoueur=Integer.toString(jeu.getJoueurAyantTour().getAge());
		String scoreJoueur=Integer.toString(jeu.getJoueurAyantTour().getScore());
		String nbCartes=Integer.toString(jeu.getJoueurAyantTour().getCartes().taille());
		vueJoueur = new VueJoueur(jeu, nomJoueur, ageJoueur, scoreJoueur, nbCartes);
		panelJoueur.add(vueJoueur);
		for (int j=0; j<this.jeu.getJoueurAyantTour().getCartes().taille(); j++) {
			Card carte=this.jeu.getJoueurAyantTour().getCartes().getCarte(j);
			VueCarte vc = new VueCarte(carte, 0, 0, jeu.getTypeJeu());
			panelJoueur.add(vc);

		}	
	}

	public void mettreAJourPanelTable() {
		panelTable.removeAll();
		
		vueClassement = new VueClassement(jeu);
		panelTable.add(vueClassement);
		for (int j=0; j<this.jeu.getTables().taille(); j++) {

			Card carte=this.jeu.getTables().getCarte(j);
			VueCarteTable vTable = null;

			if(j==0) {
				this.nbButton=1;
				vTable = new VueCarteTable(carte, 0, 0, jeu.getTypeJeu(), Integer.toString(this.nbButton),  Integer.toString(this.nbButton+1), jeu, this);
				this.nbButton+=2;

			}
			else {
				vTable = new VueCarteTable(carte, 0, 0, jeu.getTypeJeu(), Integer.toString(this.nbButton), jeu, this);
				this.nbButton++;
			}


			panelTable.add(vTable);


		}
	}



}
