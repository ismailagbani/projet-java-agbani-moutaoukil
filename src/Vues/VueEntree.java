package Vues;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import modele.Jeu;

/**
 * Classe modelisant la vue affich�e lors du lancement du jeu
 */
public class VueEntree extends Vue {
	private JComboBox menuJeu = new JComboBox();
	private JComboBox nbJoueurs= new JComboBox();
	private JComboBox categories = new JComboBox();
	private JButton valider = new JButton("Valider");
	private Jeu jeu;
	private JFrame fen;
	JMenuBar barreMenu;
	
	JMenu menuFichier;
	JMenuItem itemChargerJeu;
	
	JMenu menuHelp;	
	JMenuItem itemAideTimeline;
	JMenuItem itemAideCardline;	

	
	public VueEntree(JFrame f) {
		this.barreMenu=new JMenuBar();
		
		this.menuFichier=new JMenu("Fichier");
		this.itemChargerJeu=new JMenuItem("Charger jeu");
		
		this.menuFichier.add(itemChargerJeu);
		
		this.menuHelp=new JMenu("Help");
		this.itemAideTimeline=new JMenuItem("R�gles Timeline");	
		this.itemAideTimeline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					File f=new File("data/regle.pdf");
					Desktop bureau = Desktop.getDesktop();
					bureau.open(f);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
		this.itemAideCardline=new JMenuItem("R�gles Cardline");	
		this.itemAideCardline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					File f=new File("data/reglecardline.pdf");
					Desktop bureau = Desktop.getDesktop();
					bureau.open(f);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});

		this.menuHelp.add(itemAideTimeline);
		this.menuHelp.add(itemAideCardline);

		
		this.barreMenu.add(menuFichier);
		this.barreMenu.add(menuHelp);
	
	

		this.fen=f;
		this.itemChargerJeu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					Jeu j = Jeu.chargerJeu();
					System.out.println(j.getTypeJeu());
					Fenetre.changerVue(f, new VueJeu(j, f));
					JOptionPane.showMessageDialog(null, "Le jeu a �t� charg� avec succ�s.", "Chargement Jeu", JOptionPane.INFORMATION_MESSAGE);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
		this.fen.setJMenuBar(barreMenu);
		this.menuJeu.addItem("Choisir le jeu");
		this.menuJeu.addItem("cardline");
		this.menuJeu.addItem("timeline");
		nbJoueurs.addItem("2");
		nbJoueurs.addItem("3");
		nbJoueurs.addItem("4");
		nbJoueurs.addItem("5");
		nbJoueurs.addItem("6");
		nbJoueurs.addItem("7");
		nbJoueurs.addItem("8");
		this.add(menuJeu);
		this.add(nbJoueurs);
		this.add(categories);
		this.add(valider);

		this.menuJeu.setPreferredSize(new Dimension(200,24));
		this.nbJoueurs.setPreferredSize(new Dimension(50,24));
		this.categories.setPreferredSize(new Dimension(200,24));

		this.menuJeu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (menuJeu.getSelectedItem().equals("cardline")) {
					System.out.println("cardline");
					categories.removeAllItems();
					categories.addItem("PIB");
					categories.addItem("Superficie");
					categories.addItem("Population");
					categories.addItem("Pollution");


				} else if (menuJeu.getSelectedItem().equals("timeline")) {
					System.out.println("timeline");
					categories.removeAllItems();
					categories.addItem("Ann�e");

				}

				else {
					categories.removeAllItems();

				}

			}


		});
		this.valider.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println("nbjoueurs "+nbJoueurs.getSelectedItem().toString());
				try {
					jeu=new Jeu(categories.getSelectedItem().toString(), menuJeu.getSelectedItem().toString());
				}catch(NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Choisir un jeu", "Erreur", JOptionPane.ERROR_MESSAGE);

				}
				try {
					jeu.setNbJoueurs(Integer.parseInt(nbJoueurs.getSelectedItem().toString()));
					Fenetre.changerVue(fen, new VueCreationJoueurs(jeu, fen));
					repaint();
				}catch(NumberFormatException e) {
					JOptionPane.showMessageDialog(null, "Entrez un chiffre entre 2 et 6", "Erreur", JOptionPane.ERROR_MESSAGE);
				}


			}

		});

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

	}

	public JComboBox getMenuJeu() {
		return menuJeu;
	}

	public void setMenuJeu(JComboBox menuJeu) {
		this.menuJeu = menuJeu;
	}

	public JComboBox getNbJoueurs() {
		return nbJoueurs;
	}

	public void setNbJoueurs(JComboBox nbJoueurs) {
		this.nbJoueurs = nbJoueurs;
	}

	public JButton getValider() {
		return valider;
	}

	public void setValider(JButton valider) {
		this.valider = valider;
	}






}
