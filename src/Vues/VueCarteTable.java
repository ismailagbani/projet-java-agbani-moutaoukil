package Vues;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import Controleurs.ControleurTable;
import modele.Card;
import modele.Jeu;

/**
 * Classe representant les cartes sur la table
 */
public class VueCarteTable extends JPanel {

	private Card carte;
	private VueJeu vj;
	private JRadioButton button1;
	private JRadioButton button2;
	private int x;
	private int y;
	private String typeJeu;
	private Jeu jeu;
	private ButtonGroup groupeButton;
	private JPanel carteMilieu;
	private JLabel imgLabel;
	public VueCarteTable(Card c, int a, int b,  String type, String b1, String b2, Jeu j, VueJeu vueJ) {
		this.carte=c;
		this.x=a;
		this.y=b;
		this.typeJeu=type;
		this.jeu=j;
		carteMilieu=new JPanel();
		this.button1= new JRadioButton(b1);
		this.button2 = new JRadioButton(b2);
		groupeButton = new ButtonGroup();
		groupeButton.add(button1);
		groupeButton.add(button2);
		this.setPreferredSize(new Dimension(240, 300));
		this.setLayout(new BorderLayout());
		this.add(button1, BorderLayout.WEST);
		this.add(button2, BorderLayout.EAST);
		this.imgLabel=new JLabel();
		this.add(imgLabel, BorderLayout.CENTER);
		//this.add(carteMilieu, BorderLayout.CENTER);
		this.vj=vueJ;
		this.button1.addActionListener(new ControleurTable(this, jeu, vj));
		this.button2.addActionListener(new ControleurTable(this, jeu, vj));
		
		
		
	}
	
	public VueCarteTable(Card c, int a, int b,  String type, String b1, Jeu j, VueJeu vueJ) {
		this.vj=vueJ;
		this.jeu=j;
		this.carte=c;
		this.x=a;
		this.y=b;
		this.typeJeu=type;
		this.button1= new JRadioButton(b1);
		groupeButton = new ButtonGroup();
		groupeButton.add(button1);
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(230, 300));
		this.imgLabel=new JLabel();
		this.add(imgLabel, BorderLayout.CENTER);
		this.add(button1, BorderLayout.EAST);		
		this.button1.addActionListener(new ControleurTable(this, jeu, vj));
		this.button2 = new JRadioButton();

		//this.addMouseListener(new ControleurCarte(this));
		
	}
	
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image imgCarte=null;
		if (this.typeJeu.equals("timeline")) {
		try {
			imgCarte = carte.lireImageDate("data/timeline/cards/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	} else {
		try {
			imgCarte = carte.lireImageReponse("data/cardline/cards/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
		//g.drawImage(imgCarte, x, y, 180, 300, null);
		
		ImageIcon imgIcon = new ImageIcon(new ImageIcon(imgCarte).getImage().getScaledInstance(180, 300, Image.SCALE_DEFAULT));
		this.imgLabel.setIcon(imgIcon);
	}

	private Card getCarte() {
		return carte;
	}

	private void setCarte(Card carte) {
		this.carte = carte;
	}

	public JRadioButton getButton1() {
		return button1;
	}

	public void setButton1(JRadioButton button1) {
		this.button1 = button1;
	}

	public JRadioButton getButton2() {
		return button2;
	}

	public void setButton2(JRadioButton button2) {
		this.button2 = button2;
	}

	

	public ButtonGroup getGroupeButton() {
		return groupeButton;
	}

	private void setGroupeButton(ButtonGroup groupeButton) {
		this.groupeButton = groupeButton;
	}


	

}
