package Vues;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import modele.Jeu;
import modele.Joueur;

/**
 * Vue permettant d'entrer les donnees sur les joueurs
 */
public class VueCreationJoueurs extends Vue {

	private Jeu jeu;
	private JFrame fenetre;
	private ArrayList<Joueur> lesPlayers;
	private JButton valider = new JButton("Valider Infos");
	private JButton play = new JButton("Jouer");
	private int nbJoueurs;
	
	public VueCreationJoueurs(Jeu j, JFrame f) {
		this.jeu=j;
		
		this.fenetre=f;
		this.lesPlayers=new ArrayList<Joueur>();
		jeu.setLesJoueurs(lesPlayers);
		try {
			jeu.chargerCartes();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i=0; i<j.getNbJoueurs(); i++) {
			JLabel ps = new JLabel("Pseudo");
			JTextArea pseudo = new JTextArea("oui" + i);
			pseudo.setDragEnabled(true);
			JLabel ag = new JLabel("Age");
			JTextArea age = new JTextArea("3");
			this.add(ps);
			this.add(new JScrollPane(pseudo));
			this.add(ag);
			this.add(age);
			this.add(valider);
			this.add(play);
			pseudo.setPreferredSize(new Dimension(200,24));
			age.setPreferredSize(new Dimension(50,24));
			valider.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					Joueur player = new Joueur(pseudo.getText(), Integer.parseInt(age.getText().trim()));
					lesPlayers.add(player);
					
				}
				
			});
		}
			play.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					jeu.distribuerCartesJoueurDebutJeu();
					jeu.chargerCarteTables();
					jeu.attribuerPremierTour();
					System.out.println(jeu.getTypeJeu());
					Fenetre.changerVue(fenetre, new VueJeu(jeu, fenetre));
					repaint();
				}
				
			});
		
		System.out.println("liste des joueurs zzzzz "+lesPlayers);
		System.out.println("nbjoueurs creation "+j.getNbJoueurs());
	}
	public int getNbJoueurs() {
		return nbJoueurs;
	}
	public void setNbJoueurs(int nbJoueurs) {
		this.nbJoueurs = nbJoueurs;
	}
	public Jeu getJeu() {
		return jeu;
	}
	public void setJeu(Jeu jeu) {
		this.jeu = jeu;
	}
	
}
