package Vues;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controleurs.ControleurCarte;
import modele.Card;

/**
 * Classe permettant de gerer la vue d'une carte
 */
public class VueCarte extends JPanel {
	private Card carte;
	
	/**
	 * Coordonees de la carte
	 */
	private int x;
	private int y;
	
	
	private String typeJeu;
	private JLabel imgCarteI;
	
	public VueCarte(Card c, int a, int b, String type) {
		super();
		this.carte=c;
		this.x=a;
		this.y=b;
		this.typeJeu=type;
		this.imgCarteI=new JLabel();
		this.setPreferredSize(new Dimension(180, 300));
		this.add(imgCarteI);
		this.addMouseListener(new ControleurCarte(this));
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image imgCarte=null;
		if (this.typeJeu.equals("timeline")) {
		try {
			imgCarte = carte.lireImageDate("data/timeline/cards/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	} else {
		try {
			imgCarte = carte.lireImageReponse("data/cardline/cards/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		ImageIcon imgIcon = new ImageIcon(new ImageIcon(imgCarte).getImage().getScaledInstance(180, 300, Image.SCALE_DEFAULT));
		this.imgCarteI.setIcon(imgIcon);
	}
	
	public Card getCarte() {
		return carte;
	}
	public void setCarte(Card carte) {
		this.carte = carte;
	}

}
