package Vues;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.*;

import modele.Card;
import modele.CarteInvention;

/**
 * Classe principale 
 * pour jouer avec une interface graphique
 */
public class Fenetre {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		JFrame fenetre = new JFrame("Jeu de Cartes");
		
//		Jeu j = new Jeu("invention", 3, "timeline");
		//Jeu j = new Jeu("invention", "timeline");
		
		VueEntree vueEntree = new VueEntree(fenetre);
		fenetre.setContentPane(vueEntree);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setSize(new Dimension(800, 600));
		fenetre.setVisible(true);
		vueEntree.setFocusable(true);
		vueEntree.requestFocusInWindow();
	}
	public static void changerVue(JFrame f, Vue vue) {
		f.setContentPane(vue);
		f.revalidate();
	}
}
