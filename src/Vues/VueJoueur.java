package Vues;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modele.Jeu;

/**
 * Classe permettant de gerer la vue du joueur
 */
public class VueJoueur extends JPanel {
	private Jeu jeu;
	private JLabel nom;
	private JLabel age;
	private JLabel score;
	private JLabel nbCartes;
	private JButton joker;
	int scoreJok;
	String scoreString;

	public VueJoueur(Jeu j, String n, String a, String s, String nb) {
		this.jeu = j;
		joker = new JButton("Joker");
		nom=new JLabel("Pseudo: "+n);
		age=new JLabel("Age: "+a);

		nbCartes=new JLabel("Nombres cartes: "+nb);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JLabel image = new JLabel(new ImageIcon("data/imageJoueur.png"));
		image.setPreferredSize(new Dimension(300,100));
		this.setPreferredSize(new Dimension(180, 300));
		this.add(nom);
		this.add(age);
		this.add(image);

		this.add(nbCartes);
		this.add(joker);
		this.scoreString=s;
		score=new JLabel("Score: "+this.scoreString);
		this.add(score);
		
			this.add(joker);
			this.joker.addActionListener(new ActionListener() {			
				@Override
				public void actionPerformed(ActionEvent e) {
					scoreJok=Integer.parseInt(scoreString);
					scoreJok=scoreJok+1;
					jeu.getJoueurAyantTour().setScore(scoreJok);
					score.setText("Score: "+ Integer.toString(scoreJok));
					jeu.getJoueurAyantTour().setJokerUtilise(true);
					remove(joker);
					revalidate();
					
				}
			});

			if (this.jeu.getJoueurAyantTour().isJokerUtilise()) {
				remove(joker);
			}

		
	}






}
