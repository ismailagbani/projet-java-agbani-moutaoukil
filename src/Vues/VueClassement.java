package Vues;


import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modele.Jeu;

/**
 * Vue permettant d'afficher le classement et les infos sur les cartes
 * @author Utilisateur
 *
 */
public class VueClassement extends JPanel {
	private Jeu jeu;
	
	public VueClassement(Jeu j) {
		this.jeu = j;
	
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JLabel classement= new JLabel("CLASSEMENT");
		this.add(classement);
		
		for (int i=0; i<this.jeu.classementJoueurs().size(); i++) {
			String pseudo=this.jeu.classementJoueurs().get(i).getPseudo();
			String score=Integer.toString(this.jeu.classementJoueurs().get(i).getScore());
			this.add(new JLabel("Pseudo: "+pseudo+" Score: "+score));
		}
		JLabel infos= new JLabel("INFORMATIONS CARTES");
		this.add(infos);
		int cartesDeck = this.jeu.getCartes().taille();
		JLabel nbCartesDeck= new JLabel("Nombres Cartes Deck: "+cartesDeck);
		this.add(nbCartesDeck);
		int cartesTables = this.jeu.getTables().taille();
		JLabel nbCartesTables= new JLabel("Nombres Cartes Tables: "+cartesTables);
		this.add(nbCartesTables);

	}

}
